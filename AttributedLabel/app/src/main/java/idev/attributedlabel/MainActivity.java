package idev.attributedlabel;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import idev.attributedlabel.helper.CameraHelper;

public class MainActivity extends AppCompatActivity {

    TextView customAttributedText;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preUI();
    }

    private void preUI()
    {
        customAttributedText = (TextView) this.findViewById(R.id.mainActivityLabelID);
        changeColorOfCharacter();
        customAwesomeFontApply();
    }


    private void changeColorOfCharacter()
    {
        Spannable wordtoSpan = new SpannableString(customAttributedText.getText().toString());

        wordtoSpan.setSpan(new ForegroundColorSpan(Color.BLUE), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        customAttributedText.setText(wordtoSpan);
    }

    private void  customAwesomeFontApply()
    {
        // Getting bottom TextView which looks like an image
        final TextView bottomEditText = (TextView) this.findViewById(R.id.mainActivityEditTextBottomID);

        Typeface font = Typeface.createFromAsset( getApplicationContext().getAssets(), "fonts/fontawesome-webfont.ttf" );
        final  Button button = (Button)findViewById( R.id.mainActivityButtonId );
        button.setTypeface(font);

        bottomEditText.setTypeface(font);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                button.setTextColor(Color.CYAN);
                bottomEditText.setTextColor(Color.GREEN);
                loadSecondActivity();
            }
        });

        Button flashBtn = (Button) this.findViewById(R.id.mainActivityFlashLightButtonId);
        flashBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toggle flash light accordingly
                CameraHelper.getInstance().toggleTorchLight(getApplicationContext());
            }
        });

    }

    private void loadSecondActivity()
    {
        Intent activityChangeIntent = new Intent(getApplicationContext(), SecondActivity.class);

        // currentContext.startActivity(activityChangeIntent);
        startActivity(activityChangeIntent);
        finish();
    }


}


