package idev.attributedlabel.helper;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;

/**
 * Created by QMCPL on 10/02/17.
 */

public class CameraHelper
{
    private static CameraHelper ourInstance = new CameraHelper();

    public static CameraHelper getInstance() {
        return ourInstance;
    }

    private CameraManager mCameraManager;
    private String mCameraId;
    private Context rContext;

    private Camera mCamera;
    Boolean ifCameraManagerTorchIsOn = false;

    @Nullable
    @RequiresApi(api = Build.VERSION_CODES.M)
    private   String getCameraID()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Log.d(TAG, "camera2 selected");
            mCameraManager = (CameraManager) rContext.getSystemService(Context.CAMERA_SERVICE);
            try {
                mCameraId = mCameraManager.getCameraIdList()[0];
                return  mCameraId;
            } catch (CameraAccessException e) {
                e.printStackTrace();
                return null;

            }

        }
        return null;

    }

    private Camera getCameraInstance() {
        // TODO Auto-generated method stub
        mCamera = null;
        try {
            mCamera = Camera.open();
        } catch (Exception e) {

        }
        return mCamera;
    }


    public  void toggleTorchLight(Context mContext)
    {
        if  (mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH) == false)
        {
            return;
        }
        rContext = mContext;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
           // Log.d(TAG, "camera2 selected");
            if (CameraHelper.getInstance().getCameraID() != null)
            {
                try {

                    String cameraID = CameraHelper.getInstance().getCameraID();

                    if (ifCameraManagerTorchIsOn == false)
                        {
                            ifCameraManagerTorchIsOn = true;
                            mCameraManager.setTorchMode(mCameraId, true);
                        }else
                        {
                            ifCameraManagerTorchIsOn = false;
                            mCameraManager.setTorchMode(mCameraId, false);
                        }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }else
        {
            if  (mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH) == false)
            {
                return;
            }
          // USE CAMERA API

            if (mCamera == null)
            {
                try
                {
                    mCamera = Camera.open(0);
                    // try to open the camera to turn on the torch
                    Camera.Parameters param = mCamera.getParameters();

                    if (mCamera.getParameters().getFlashMode().equals("torch"))
                    {
                        param.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        mCamera.setParameters(param);
                        mCamera.stopPreview();
                        mCamera.release();
                        mCamera = null;
                    }else
                    {
                        param.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                        mCamera.setParameters(param);
                        mCamera.startPreview();
                        mCamera.autoFocus(new Camera.AutoFocusCallback() {
                            public void onAutoFocus(boolean success, Camera camera) {
                            }
                        });
                    }

                } catch (Exception e)
                {

                }

            }else
            {
                // try to open the camera to turn on the torch
                Camera.Parameters param = mCamera.getParameters();
                param.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                mCamera.setParameters(param);
                mCamera.stopPreview();
                mCamera.release();
                mCamera = null;
            }

        }
    }

    public  void switchOffTorchAfterCheck(Context mContext)
    {
        if  (mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH) == false)
        {
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            // Log.d(TAG, "camera2 selected");
            if (CameraHelper.getInstance().getCameraID() != null)
            {

                try {
                    String cameraID = CameraHelper.getInstance().getCameraID();
                    mCameraManager.setTorchMode(mCameraId, false);
                    ifCameraManagerTorchIsOn = false;

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }else
        {

            if (mCamera != null)
            {
                Camera.Parameters param = mCamera.getParameters();
                param.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                mCamera.setParameters(param);
                mCamera.stopPreview();
                mCamera.release();
                mCamera = null;
            }

        }
    }
}
