package idev.attributedlabel.helper;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by QMCPL on 10/02/17.
 */

public class AppController extends Application implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {

    private static final String LOG_TAG = AppController.class.getSimpleName();
    // New variables for detecting if the application is in background
    private static String TAGState = AppController.class.getName();

    public static String stateOfLifeCycle = "";

    public static boolean wasInBackground = false;
    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(this);
        // New app state
        wasInBackground = false;
        stateOfLifeCycle = "Create";


    }


    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        Log.e(LOG_TAG, activity.getLocalClassName() + " is created");
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.e(LOG_TAG, activity.getLocalClassName() + " is started");

        // New App State
        stateOfLifeCycle = "Start";

    }

    @Override
    public void onActivityResumed(Activity activity) {
        Log.e(LOG_TAG, activity.getLocalClassName() + " is resumed");

        // New App State
        stateOfLifeCycle = "Resume";

    }

    @Override
    public void onActivityPaused(Activity activity) {

        Log.e(LOG_TAG, activity.getLocalClassName() + " is paused");

        // App State
        stateOfLifeCycle = "Pause";

    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.e(LOG_TAG, activity.getLocalClassName() + " is stopped");

        // App state
        stateOfLifeCycle = "Stop";

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        Log.e(LOG_TAG, activity.getLocalClassName() + " has saved instance state - " + bundle);
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.e(LOG_TAG, activity.getLocalClassName() + " is destroyed");
        // New App State
        wasInBackground = false;
        stateOfLifeCycle = "Destroy";

    }

    // Component
    @Override
    public void onTrimMemory(int level) {
        if (stateOfLifeCycle.equals("Stop")) {
            wasInBackground = true;
            // Toggle flash light accordingly
            CameraHelper.getInstance().switchOffTorchAfterCheck(getApplicationContext());

        }
        super.onTrimMemory(level);
    }
}