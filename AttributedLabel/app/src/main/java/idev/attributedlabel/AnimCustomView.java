package idev.attributedlabel;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by QMCPL on 13/02/17.
 */

public class AnimCustomView extends View
{


        public AnimCustomView(Context context)
        {
            super(context);
            init(null, 0);

        }

        public AnimCustomView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init(attrs, 0);

        }

        public AnimCustomView(Context context, AttributeSet attrs, int defStyleAttr)
        {
            super(context, attrs, defStyleAttr);
            init(attrs, defStyleAttr);
        }

        public void addTextView(SecondActivity secondActivity)
        {
            TextView msg = new TextView(super.getContext());
            msg.setText("Yay");
            msg.setTextSize(10);
            msg.setPadding(10, 10, 10, 10);
            msg.setTextColor(Color.CYAN);
            secondActivity.setContentView(msg);
//            msg.setLayoutParams(new ViewGroup.LayoutParams(
//                    ViewGroup.LayoutParams.MATCH_PARENT,
//                    ViewGroup.LayoutParams.MATCH_PARENT));

        }


        private  void init(AttributeSet attrs, int defStyle)
        {
            setBackgroundColor(Color.MAGENTA);

           // addTextView();
        }

}
