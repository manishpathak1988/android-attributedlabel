package idev.attributedlabel;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    private LinearLayout myLInearLayout;
    private TextView valueTV;
    private Button valueB;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        preUI();
    }

    private void preUI()
    {
        //AnimCustomView animObj = new AnimCustomView(this);
       // animObj.addTextView(this);
        final  Button button = (Button)findViewById( R.id.buttonAnimID);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loadThirdActivity();
            }
        });

    }

    @Override
    public void onBackPressed()
    {
        finish();
        loadMainActivity();
        super.onBackPressed();
    }

    private void loadMainActivity()
    {
        Intent activityChangeIntent = new Intent(getApplicationContext(), MainActivity.class);

        // currentContext.startActivity(activityChangeIntent);
        startActivity(activityChangeIntent);

    }

    private void loadThirdActivity()
    {
        Intent activityChangeIntent = new Intent(getApplicationContext(), ThirdActivity.class);

        // currentContext.startActivity(activityChangeIntent);
        startActivity(activityChangeIntent);
        finish();
    }


}
