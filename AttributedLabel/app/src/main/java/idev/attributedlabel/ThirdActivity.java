package idev.attributedlabel;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.ChangeBounds;
import android.transition.TransitionManager;
import android.view.View;
import android.widget.Button;

public class ThirdActivity extends AppCompatActivity {

    ConstraintSet mConstraintSet1 = new ConstraintSet();
    ConstraintSet mConstraintSet2 = new ConstraintSet();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mConstraintSet2.clone(getApplicationContext(), R.layout.activity_third2);
        setContentView(R.layout.activity_third);
        preUI();
    }


    private void preUI()
    {
        //AnimCustomView animObj = new AnimCustomView(this);
        // animObj.addTextView(this);

        final ConstraintLayout mConstraintLayout = (ConstraintLayout) findViewById(R.id.activity_third);
        mConstraintSet1.clone(mConstraintLayout);

        final Button button = (Button)findViewById( R.id.animBtnThirdID);

        button.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {

                final ChangeBounds transition = new ChangeBounds();
                transition.setDuration(1800L); // Sets a duration of 600 milliseconds
                TransitionManager.beginDelayedTransition(mConstraintLayout, transition);
                mConstraintSet2.applyTo(mConstraintLayout);
            }
        });
    }



    private void loadThirdActivity()
    {


    }

}
